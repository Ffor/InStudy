package com.gsp.instudy;

public class Task {

    private long id;
    private long classroom;
    private String subject;
    private String lecturer;
    private long beginingOfSubject;
    private long endingOfSubject;
    private long date;

    Task(long id, long classroom, String subject, String lecturer, long beginingOfSubject, long endingOfSubject, long date){
        this.id = id;
        this.classroom = classroom;
        this.subject = subject;
        this.lecturer = lecturer;
        this.beginingOfSubject = beginingOfSubject;
        this.endingOfSubject = endingOfSubject;
        this.date = date;
    }

    public long getId(){
        return id;
    }

    public void setClassroom(int classroom) {
        this.classroom = classroom;
    }

    public long getClassroom() {
        return classroom;
    }

    public void setLecturer(String lecturer) {
        this.lecturer = lecturer;
    }

    public String getLecturer() {
        return lecturer;
    }

    public void setBeginingOfSubject(long beginingOfSubject) {
        this.beginingOfSubject = beginingOfSubject;
    }

    public long getBeginingOfSubject() {
        return beginingOfSubject;
    }

    public void setEndingOfSubject(long endingOfSubject) {
        this.endingOfSubject = endingOfSubject;
    }

    public long getEndingOfSubject() {
        return endingOfSubject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getSubject() {
        return subject;
    }

    public long getDate(){
        return date;
    }

    public void setDate(long date){
        this.date = date;
    }
}
