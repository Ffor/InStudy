package com.gsp.instudy;

import android.app.DatePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.content.res.Resources;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ListView;
import android.widget.Toast;

import com.github.clans.fab.FloatingActionMenu;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

public class MainActivity extends AppCompatActivity {

    final static int EDIT_BUTTON = 1;
    final static int DELETE_BUTTON = 0;
    final static int NONE = -1;

    int idButton = NONE;

    long dateFromN = 0, dateToN = 0;

    DateDB dateDB;
    ListView lvMain;
    DateAdapter dateAdapter;

    Calendar calendar = Calendar.getInstance();
    SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());

    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        dateDB = new DateDB(this);

        lvMain = findViewById(R.id.lvMain);

        lvMain.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            public void onItemClick(AdapterView<?> parent, View view,
                                    int position, long id) {
                switch (idButton){
                    case EDIT_BUTTON:
                        Date aDate = dateAdapter.getItem(position);
                        Intent intent = new Intent(getApplicationContext(), DayActivity.class);
                        intent.putExtra("date", aDate.getDate());
                        startActivity(intent);
                        break;
                    case DELETE_BUTTON:
                        Date selectedDate = dateAdapter.getItem(position);
                        dateDB.open();
                        dateDB.deleteDate(selectedDate.getId(),selectedDate.getDate());
                        dateDB.close();
                        dateAdapter.remove(selectedDate);
                        dateAdapter.notifyDataSetChanged();
                        break;
                }
            }

        });
    }

    public void onClick(View v) {
        com.github.clans.fab.FloatingActionButton fab, fab2;
        switch (v.getId())
        {
            case  R.id.update:
                dateDB = new DateDB(this);

                dateDB.open();

                Button dateFromBtn = findViewById(R.id.dateFrom);
                Button dateToBtn = findViewById(R.id.dateTo);

                try {
                    java.util.Date dateFromM = dateFormat.parse(dateFromBtn.getText().toString());
                    java.util.Date dateToM = dateFormat.parse(dateToBtn.getText().toString());

                    dateFromN = dateFromM.getTime();
                    dateToN = dateToM.getTime();

                }catch (ParseException e){
                    e.printStackTrace();
                }

                List<Date> cl = dateDB.getDates(dateFromN,dateToN);
                dateAdapter = new DateAdapter(this, R.layout.item_date,cl);
                dateAdapter.notifyDataSetChanged();
                lvMain.setAdapter(dateAdapter);

                dateDB.close();
                break;
            case R.id.dateFrom:
                DataFrom dateFrom = new DataFrom();
                dateFrom.show(getSupportFragmentManager(), "datePicker");
                break;
            case R.id.dateTo:
                DataTo dateTo = new DataTo();
                dateTo.show(getSupportFragmentManager(), "datePicker");
                break;
            case  R.id.delete:
                fab = findViewById(R.id.delete);
                fab2 = findViewById(R.id.edit);
                if(idButton == DELETE_BUTTON){
                    idButton = NONE;
                    fab.setProgress(0,false);
                    fab.hideProgress();
                    Toast.makeText(this,R.string.deleteModeOff,Toast.LENGTH_LONG).show();
                    break;
                }
                fab2.setProgress(0,false);
                fab2.hideProgress();

                fab.setProgress(100,true);
                idButton = DELETE_BUTTON;

                Toast.makeText(this,R.string.deleteModeOn,Toast.LENGTH_LONG).show();
                break;
            case R.id.edit:
                fab = findViewById(R.id.edit);
                fab2 = findViewById(R.id.delete);
                if(idButton == EDIT_BUTTON){
                    idButton = NONE;
                    fab.setProgress(0,false);
                    fab.hideProgress();
                    Toast.makeText(this,R.string.editModeOff,Toast.LENGTH_LONG).show();
                    break;
                }

                fab2.setProgress(0,false);
                fab2.hideProgress();

                fab.setProgress(100,true);
                idButton = EDIT_BUTTON;

                Toast.makeText(this,R.string.editModeOn,Toast.LENGTH_LONG).show();
                break;
            case  R.id.add:
                DatePickerDialog datePickerDialog = new DatePickerDialog(this,dateCallBack,
                        calendar.get(Calendar.YEAR),calendar.get(Calendar.MONTH),calendar.get(Calendar.DAY_OF_MONTH));
                datePickerDialog.show();
                break;
        }

    }

    @Override
    public void onResume() {
        super.onResume();
        dateDB = new DateDB(this);

        dateDB.open();

        List<Date> cl = dateDB.getDates(dateFromN,dateToN);
        dateAdapter = new DateAdapter(this, R.layout.item_date,cl);
        lvMain.setAdapter(dateAdapter);
        dateDB.close();
    }

    @Override
    public void onDestroy()
    {
        super.onDestroy();
        dateDB.close();
    }

    DatePickerDialog.OnDateSetListener dateCallBack = new DatePickerDialog.OnDateSetListener() {
        @Override
        public void onDateSet(DatePicker view, int year, int month, int dayOfMonth) {
            calendar.set(Calendar.YEAR,year);
            calendar.set(Calendar.MONTH,month);
            calendar.set(Calendar.DAY_OF_MONTH,dayOfMonth);
            calendar.set(Calendar.HOUR_OF_DAY,0);
            calendar.set(Calendar.SECOND,0);
            calendar.set(Calendar.MILLISECOND, 0);

            long date = calendar.getTimeInMillis();

            dateDB.open();
            boolean isExist = dateDB.checkIfExist(date);
            dateDB.close();

            if(!isExist) {
                dateDB.open();
                dateDB.addDate(new Date(0, date));
                dateDB.close();

                Intent intent = new Intent(getApplicationContext(), DayActivity.class);
                intent.putExtra("date", date);
                startActivity(intent);
            }
            else {
                showWarning();
            }
        }
    };

    void showWarning(){
        Toast.makeText(this,R.string.dateIsExist,Toast.LENGTH_LONG).show();
    }
}
