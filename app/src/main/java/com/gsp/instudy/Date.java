package com.gsp.instudy;

import java.sql.Time;

/**
 * Created by ffor4 on 09.05.2018.
 */

public class Date {

    long id;
    long date;
    long countOfTasks;

    Date(long  id, long _date){
        this.id = id;
        date = _date;
    }

    public long getId() {
        return id;
    }

    public void setDate(int date) {
        this.date = date;
    }

    public long getDate() {
        return date;
    }

    public void setCountOfTasks(long countOfTasks) {
        this.countOfTasks = countOfTasks;
    }

    public long getCountOfTasks() {
        return countOfTasks;
    }
}
