package com.gsp.instudy;


import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

public class DateAdapter extends ArrayAdapter<Date> {

    private LayoutInflater inflater;
    private int layout;
    private List<Date> dates;

    SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM.yyyy", Locale.getDefault());
    SimpleDateFormat dayFormat = new SimpleDateFormat("EEEE", Locale.getDefault());

    Context context;

    public DateAdapter(Context context, int resource, List<Date> dates){

        super(context,resource, dates);
        this.context = context;
        this.dates = dates;
        this.layout = resource;
        this.inflater = LayoutInflater.from(context);
    }

    static class ViewHolder{

        public TextView date;
        public TextView nameOfDate;
        public TextView countOfTasks;
    }

    public View getView(int position, View convertView, ViewGroup parent){

        ViewHolder viewHolder;

        View rowView = convertView;

        if(rowView == null){
            rowView = inflater.inflate(this.layout,parent,false);
            viewHolder = new ViewHolder();
            viewHolder.date = (TextView)rowView.findViewById(R.id.date);
            viewHolder.nameOfDate = (TextView)rowView.findViewById(R.id.nameOfDay);
            viewHolder.countOfTasks = rowView.findViewById(R.id.countOfTasks);
            rowView.setTag(viewHolder);
        }
        else{
            viewHolder = (ViewHolder)rowView.getTag();
        }

        Date aDate = dates.get(position);

        viewHolder.date.setText(dateFormat.format(new java.util.Date(aDate.getDate())));
        viewHolder.nameOfDate.setText(dayFormat.format(new java.util.Date(aDate.getDate())));

        Resources res = context.getResources();

        viewHolder.countOfTasks.setText(res.getString(R.string.count) + String.valueOf(aDate.getCountOfTasks()));

        return rowView;
    }

}
