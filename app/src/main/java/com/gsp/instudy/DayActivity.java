package com.gsp.instudy;


import android.annotation.SuppressLint;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

import com.baoyz.swipemenulistview.SwipeMenu;
import com.baoyz.swipemenulistview.SwipeMenuCreator;
import com.baoyz.swipemenulistview.SwipeMenuItem;
import com.baoyz.swipemenulistview.SwipeMenuListView;

import java.text.SimpleDateFormat;
import java.util.List;
import java.util.Locale;

public class DayActivity extends AppCompatActivity{

    private ListView taskList;
    private TaskDB taskDB;
    private TextView dateD;

    TaskAdapter taskAdapter;

    SimpleDateFormat dateFormat = new SimpleDateFormat("dd.MM", Locale.getDefault());
    SimpleDateFormat dayFormat = new SimpleDateFormat("EEEE", Locale.getDefault());

    long date;

    @SuppressLint("CutPasteId")
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_day);

        taskDB = new TaskDB(this);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            date = extras.getLong("date");
        }

        dateD = findViewById(R.id.dateD);
        dateD.setText(dateFormat.format(date) + " — " + dayFormat.format(date));

        @SuppressLint("CutPasteId") SwipeMenuListView listView = (SwipeMenuListView) findViewById(R.id.tasksList);

        taskList = (ListView)findViewById(R.id.tasksList);

        SwipeMenuCreator creator = new SwipeMenuCreator() {

            @Override
            public void create(SwipeMenu menu) {

                menu.addMenuItem(customEditItem());

                menu.addMenuItem(customDeleteItem());
            }
        };

        listView.setMenuCreator(creator);

        listView.setOnMenuItemClickListener(new SwipeMenuListView.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(int position, SwipeMenu menu, int index) {
                switch (index) {
                    case 0:
                        Task task = taskAdapter.getItem(position);
                        if(task!=null) {
                            Intent intent = new Intent(getApplicationContext(), TaskActivity.class);
                            intent.putExtra("id", task.getId());
                            intent.putExtra("date", task.getDate());
                            startActivity(intent);
                        }
                        break;
                    case 1:
                        Task selectedTask = taskAdapter.getItem(position);
                        deleteTaskFromDBandList(selectedTask);
                        break;
                }
                return false;
            }
        });

    }

    @Override
    public void onResume() {
        super.onResume();
        TaskDB taskDB = new TaskDB(this);
        taskDB.open();

        List<Task> tasks = taskDB.getTasks(date);

        taskAdapter = new TaskAdapter(this, R.layout.list_task, tasks);
        taskList.setAdapter(taskAdapter);
        taskDB.close();
    }

    public void add(View view){
        Intent intent = new Intent(this, TaskActivity.class);
        intent.putExtra("date",date);
        startActivity(intent);
    }

    private void deleteTaskFromDBandList(Task selectedTask){
        taskDB.open();
        taskDB.deleteTask(selectedTask.getId());
        taskDB.close();
        taskAdapter.remove(selectedTask);
        taskAdapter.notifyDataSetChanged();
    }

    private SwipeMenuItem customEditItem(){

        SwipeMenuItem editItem = new SwipeMenuItem(
                getApplicationContext());
        editItem.setBackground(new ColorDrawable(Color.rgb(0xD1,
                0xCA, 0x14)));
        editItem.setWidth(170);
        editItem.setIcon(R.drawable.ic_edit);

        return editItem;
    }

    private SwipeMenuItem customDeleteItem(){

        SwipeMenuItem deleteItem = new SwipeMenuItem(
            getApplicationContext());
        deleteItem.setBackground(new ColorDrawable(Color.rgb(0xFF,
                0x00, 0x00)));
        deleteItem.setWidth(170);
        deleteItem.setIcon(R.drawable.ic_delete);

        return deleteItem;
    }
}
