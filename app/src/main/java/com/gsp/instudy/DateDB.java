package com.gsp.instudy;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by ffor4 on 14.05.2018.
 */

public class DateDB {

    private static final String DATABASE_TABLE_DATE = "date";
    private static final String DATABASE_TABLE_TASKS = "tasks";

    private static final String COLUMN_ID = "_id";
    private static final String COLUMN_DATE = "date";

    private final Context context;

    private DatabaseHelper databaseHelper;
    private SQLiteDatabase database;

    public DateDB(Context context){

        this.context = context;
    }


    public void open(){
        databaseHelper = new DatabaseHelper(context);
        database = databaseHelper.getWritableDatabase();
    }

    public void close(){
        if(databaseHelper!=null) databaseHelper.close();
    }

    public Cursor getAllDates(){
        return database.query(DATABASE_TABLE_DATE,null,null,null,null,null,COLUMN_DATE);
    }

    public void addDate(Date aDate){

        ContentValues cv = new ContentValues();

        cv.put(COLUMN_DATE, aDate.getDate());

        database.insert(DATABASE_TABLE_DATE,null,cv);
        database.query(DATABASE_TABLE_DATE,null,null,null,null,null,COLUMN_DATE);

    }

    public void deleteDate(long id, long date){

        database.delete(DATABASE_TABLE_TASKS,COLUMN_DATE + " = " + date,null);
        database.delete(DATABASE_TABLE_DATE,COLUMN_ID + " = " + id,null);
        database.query(DATABASE_TABLE_DATE,null,null,null,null,null,COLUMN_DATE);

    }

    public Date getDate(long id){

        Date aDate = null;

        String query = String.format("SELECT * FROM %s Where %s=?", DATABASE_TABLE_DATE,COLUMN_ID);
        Cursor cursor = database.rawQuery(query,new String[]{String.valueOf(id)});
        if(cursor.moveToFirst()){
            long date = cursor.getInt(cursor.getColumnIndex(COLUMN_DATE));
            aDate = new Date(id,date);
        }
        cursor.close();
        return aDate;

    }

    public boolean checkIfExist(long date){

        String query = String.format("SELECT * FROM %s Where %s=?", DATABASE_TABLE_DATE,COLUMN_DATE);
        Cursor cursor = database.rawQuery(query, new String[]{String.valueOf(date)});
        if(cursor.moveToFirst())
            return true;
        return false;
    }

    public List<Date> getDates(long dateFrom, long dateTo){
        ArrayList<Date> dates = new ArrayList<>();
        Cursor cursor;
        if(dateFrom > 0 || dateTo > 0) {
            cursor = getDateByDate(dateFrom, dateTo);
        } else {
            cursor = getAllDates();
        }
        if(cursor.moveToFirst()){
            do{
                long id = cursor.getLong(cursor.getColumnIndex(COLUMN_ID));
                long date = cursor.getLong(cursor.getColumnIndex(COLUMN_DATE));

                String sFunc = "count(*) as Count";

                String columns[]={sFunc};
                String selection = "date = ?";
                String selectionArgs[]={String.valueOf(date)};

                Cursor chck = database.query(DATABASE_TABLE_TASKS,columns,selection,selectionArgs,null,null,null);


                long count = 0;

                if(chck.moveToFirst()){
                    count= chck.getLong(chck.getColumnIndex("Count"));
                }
                chck.close();

                Date temp = new Date(id,date);
                temp.setCountOfTasks(count);
                dates.add(temp);
            }
            while(cursor.moveToNext());
        }
        cursor.close();
        return dates;
    }

    public Cursor getDateByDate(long dateFrom, long dateTo){
        String selection = "";

        if(dateFrom > 0)
            selection = "date >= " + String.valueOf(dateFrom);
        if(dateTo > 0)
            selection = "date <= " + String.valueOf(dateTo);
        if(dateTo > 0 && dateFrom > 0)
            selection = "date BETWEEN " + String.valueOf(dateFrom) + " AND " + String.valueOf(dateTo);

        return database.query(DATABASE_TABLE_DATE, null,selection,null,null,null,COLUMN_DATE);
    }

}
