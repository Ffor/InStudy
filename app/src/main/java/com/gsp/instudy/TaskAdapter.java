package com.gsp.instudy;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.gsp.instudy.Task;

import java.sql.Date;
import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.List;

public class TaskAdapter extends ArrayAdapter<Task>{

    private LayoutInflater inflater;
    private int layout;
    private List<Task> tasks;

    @SuppressLint("SimpleDateFormat")
    private SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");

    TaskAdapter(Context context, int resource, List<Task> tasks){

        super(context,resource,tasks);
        this.tasks = tasks;
        this.layout = resource;
        this.inflater = LayoutInflater.from(context);
    }

    static class ViewHolder{

        public TextView classroom;
        public TextView lecturer;
        public TextView subject;
        TextView beginOfSubject;
        TextView endOfSubject;
    }

    @NonNull
    public View getView(int position, View convertView, @NonNull ViewGroup parent){

        ViewHolder viewHolder;

        View rowView = convertView;

        if(rowView == null){
            rowView = inflater.inflate(this.layout,parent,false);
            viewHolder = createNewViewHolder(rowView);
            rowView.setTag(viewHolder);
        }
        else{
            viewHolder = (ViewHolder)rowView.getTag();
        }

        Task task = tasks.get(position);

        fillViewByTask(viewHolder,task);

        return rowView;
    }


    private ViewHolder createNewViewHolder(View rowView)
    {
        ViewHolder newViewHolder= new ViewHolder();
        newViewHolder.classroom = rowView.findViewById(R.id.classroom);
        newViewHolder.lecturer = rowView.findViewById(R.id.lecturer);
        newViewHolder.subject = rowView.findViewById(R.id.subject);
        newViewHolder.beginOfSubject = rowView.findViewById(R.id.beginingOfSubject);
        newViewHolder.endOfSubject = rowView.findViewById(R.id.endingOfSubject);

        return  newViewHolder;
    }

    private void fillViewByTask(ViewHolder viewHolder, Task task){

        viewHolder.classroom.setText(String.valueOf(task.getClassroom()));
        viewHolder.lecturer.setText(task.getLecturer());
        viewHolder.subject.setText(task.getSubject());
        viewHolder.beginOfSubject.setText(simpleDateFormat.format(new Time(task.getBeginingOfSubject()).getTime()));
        viewHolder.endOfSubject.setText(simpleDateFormat.format(new Time(task.getEndingOfSubject()).getTime()));
    }
}
