package com.gsp.instudy;

import android.annotation.SuppressLint;
import android.app.Dialog;
import android.app.TimePickerDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.TimePicker;

import java.sql.Time;
import java.text.SimpleDateFormat;
import java.util.Calendar;

public class TaskActivity extends AppCompatActivity{

    Time time;

    EditText classroom;
    EditText lecturer;
    EditText subject;

    TextView beginOfSubject;
    TextView endOfSubject;

    TextInputLayout tilClassroom;
    TextInputLayout tilSubject;
    TextInputLayout tilLecturer;

    ImageView imgBeginTime;
    ImageView imgEndTime;

    Button saveButton;
    Button deleteButton;

    int DIALOG_BEGINTIME = 1;
    int DIALOG_ENDTIME = 2;

    @SuppressLint("SimpleDateFormat")
    SimpleDateFormat simpleDateFormat = new SimpleDateFormat("HH:mm:ss");

    private TaskDB taskDB;
    private long taskId=0;

    Calendar calendar = Calendar.getInstance();

    long newBeginTimeOfSubject;
    long newEndTimeOfSubject;

    boolean isUsedTimePickerBegin = false;
    boolean isUsedTimePickerEnd = false;
    boolean isExistTask = false;
    boolean isAllRight = true;

    long date;

    Task task;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_task);

        setFieldsByID();

        taskDB = new TaskDB(this);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            taskId = extras.getLong("id");
            date = extras.getLong("date");
        }

        isExistTask = checkExistTask(taskId);

        if (isExistTask) {
            fillViewsByTask();
            imgBeginTime.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_check));
            imgEndTime.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_check));
        } else {
            deleteButton.setVisibility(View.GONE);
        }
    }

    public void save(View view){

        isAllRight = true;

        int newClassroom = 0;
        String newSubject = null;
        String newLecturer = null;

        if(classroom.getText().toString().isEmpty())
        {
            isAllRight = false;
            tilClassroom.setError(getString(R.string.inputError));
        }
        else
        {
            newClassroom = Integer.parseInt(classroom.getText().toString());
            tilClassroom.setError(null);
        }

        if(subject.getText().toString().isEmpty())
        {
            isAllRight = false;
            tilSubject.setError(getString(R.string.inputError));
        }
        else
        {
            newSubject = subject.getText().toString();
            tilSubject.setError(null);
        }

        if(lecturer.getText().toString().isEmpty())
        {
            isAllRight = false;
            tilLecturer.setError(getString(R.string.inputError));
        }
        else
        {
            newLecturer = lecturer.getText().toString();
            tilLecturer.setError(null);
        }

        setBeginAndEndTime();

        if(!isAllRight)
            return;

        Task newTask = new Task(taskId, newClassroom, newSubject, newLecturer, newBeginTimeOfSubject, newEndTimeOfSubject,date);

        taskDB.open();

        if (isExistTask)
            taskDB.changeTask(taskId,newTask);
        else
            taskDB.addTask(newTask);

        taskDB.close();
        goHome();
    }
    public void delete(View view){

        taskDB.open();
        taskDB.deleteTask(taskId);
        taskDB.close();
        goHome();
    }
    private void goHome(){

        Intent intent = new Intent(this, DayActivity.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP | Intent.FLAG_ACTIVITY_SINGLE_TOP);
        startActivity(intent);
    }

    public void setBeginTime(View v) {
        showDialog(DIALOG_BEGINTIME);
    }

    public void setEndTime(View v) {
        showDialog(DIALOG_ENDTIME);
    }

    protected Dialog onCreateDialog(int id) {
        if (id == DIALOG_BEGINTIME) {
            return createTimePicker(this,beginCallBack);
        }
        if (id == DIALOG_ENDTIME) {
            return createTimePicker(this,endCallBack);
        }
        return super.onCreateDialog(id);
    }

    TimePickerDialog.OnTimeSetListener beginCallBack = new TimePickerDialog.OnTimeSetListener() {
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

            setTime(hourOfDay,minute);

            newBeginTimeOfSubject = calendar.getTimeInMillis();

            beginOfSubject.setText(simpleDateFormat.format(new Time (newBeginTimeOfSubject)));

            isUsedTimePickerBegin = true;
        }
    };

    TimePickerDialog.OnTimeSetListener endCallBack = new TimePickerDialog.OnTimeSetListener() {
        public void onTimeSet(TimePicker view, int hourOfDay, int minute) {

            setTime(hourOfDay,minute);

            newEndTimeOfSubject = calendar.getTimeInMillis();

            endOfSubject.setText(simpleDateFormat.format(new Time (newEndTimeOfSubject)));

            isUsedTimePickerEnd = true;
        }
    };

    private void setFieldsByID(){
        classroom = findViewById(R.id.classroom);
        lecturer = findViewById(R.id.lecturer);
        subject = findViewById(R.id.subject);
        beginOfSubject = findViewById(R.id.beginingOfSubject);
        endOfSubject = findViewById(R.id.endingOfSubject);
        deleteButton = findViewById(R.id.deleteButton);
        saveButton = findViewById(R.id.saveButton);

        tilLecturer = findViewById(R.id.tilLecturer);
        tilSubject = findViewById(R.id.tilSubject);
        tilClassroom = findViewById(R.id.tilClassroom);

        imgBeginTime = findViewById(R.id.imgBeginTime);
        imgEndTime = findViewById(R.id.imgEndTime);
    }

    private void fillViewsByTask(){

        taskDB.open();

        task = taskDB.getTask(taskId);

        classroom.setText(String.valueOf(task.getClassroom()));
        subject.setText(task.getSubject());
        lecturer.setText(task.getLecturer());
        beginOfSubject.setText(simpleDateFormat.format(new Time(task.getBeginingOfSubject())));
        endOfSubject.setText(simpleDateFormat.format(new Time(task.getEndingOfSubject())));

        taskDB.close();
    }

    private boolean checkExistTask(long taskId){
        return taskId>0;
    }

    private void setBeginAndEndTime(){

        if(!isUsedTimePickerBegin) {
            if(!isExistTask)
            {
                isAllRight = false;
                imgBeginTime.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_close));
            }
            else {
                newBeginTimeOfSubject = task.getBeginingOfSubject();
                imgBeginTime.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_check));
            };
        }
        else
            imgBeginTime.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_check));
        if(!isUsedTimePickerEnd) {
            if(!isExistTask)
            {
                isAllRight = false;
                imgEndTime.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_close));
            }
            else {
                newEndTimeOfSubject = task.getBeginingOfSubject();
                imgEndTime.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_check));
            }
        }
        else
            imgEndTime.setImageDrawable(ContextCompat.getDrawable(this, R.drawable.ic_check));
    }

    private void setTime(int hourOfDay, int minute){

        calendar.set(Calendar.HOUR_OF_DAY, hourOfDay);
        calendar.set(Calendar.MINUTE, minute);
        calendar.set(Calendar.SECOND, 0);
        calendar.set(Calendar.MILLISECOND, 0);
    }

    private TimePickerDialog createTimePicker(Context context, TimePickerDialog.OnTimeSetListener callBack){
        if(isExistTask)
            return new TimePickerDialog(context, callBack, new Time(task.getEndingOfSubject()).getHours(), new Time(task.getEndingOfSubject()).getMinutes(), true);
        else
            return new TimePickerDialog(context,callBack,0,0,true);
    }
}

