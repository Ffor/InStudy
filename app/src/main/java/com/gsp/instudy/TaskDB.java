package com.gsp.instudy;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;

import java.util.ArrayList;
import java.util.List;

public class TaskDB {

    private static final String DATABASE_TABLE = "tasks";

    private static final String COLUMN_ID = "_id";
    private static final String COLUMN_CLASSROOM = "classroom";
    private static final String COLUMN_SUBJECT = "subject";
    private static final String COLUMN_LECTURER = "lecturer";
    private static final String COLUMN_BEGINOFSUBJECT = "beginofsubject";
    private static final String COLUMN_ENDOFSUBJECT = "endofsubject";
    private static final String COLUMN_DATE = "date";

    private final Context context;

    private DatabaseHelper databaseHelper;
    private SQLiteDatabase database;

    TaskDB(Context context){

        this.context = context;
    }

    public void open(){

        databaseHelper = new DatabaseHelper(context);
        database = databaseHelper.getWritableDatabase();
    }

    public void close(){

        if(databaseHelper!=null) databaseHelper.close();
    }

    private Cursor getAllTasks(long date){

        String selection = "date = ?";
        String selectionArgs[]={String.valueOf(date)};

        return database.query(DATABASE_TABLE,null,selection,selectionArgs,null,null,COLUMN_BEGINOFSUBJECT);
    }

    public void changeTask(long id, Task newTask){

        ContentValues cv = createContentValuesFor(newTask);

        String selection = "date = ?";
        String selectionArgs[]={String.valueOf(newTask.getDate())};

        database.update(DATABASE_TABLE, cv, COLUMN_ID + " = " + id, null);
        database.query(DATABASE_TABLE,null,selection,selectionArgs,null,null,COLUMN_BEGINOFSUBJECT);
    }

    public void deleteTask(long id){

        database.delete(DATABASE_TABLE, COLUMN_ID + " = " + id, null);
        database.query(DATABASE_TABLE,null,null,null,null,null,COLUMN_BEGINOFSUBJECT);
    }

    public void addTask(Task task){

        ContentValues cv = createContentValuesFor(task);

        String selection = "date = ?";
        String selectionArgs[]={String.valueOf(task.getDate())};

        database.insert(DATABASE_TABLE,null, cv);
        database.query(DATABASE_TABLE,null,selection,selectionArgs,null,null,COLUMN_BEGINOFSUBJECT);
    }

    public List<Task> getTasks(long date){

        ArrayList<Task> tasks = new ArrayList<>();

        Cursor cursor = getAllTasks(date);

        if(cursor.moveToFirst()){

            do{

                tasks.add(getTaskFromDB(cursor,-1));
            }
            while (cursor.moveToNext());
        }

        cursor.close();

        return  tasks;
    }

    public Task getTask(long id){

        Task task = null;

        String query = String.format("SELECT * FROM %s WHERE %s=?",DATABASE_TABLE, COLUMN_ID);

        Cursor cursor = database.rawQuery(query, new String[]{ String.valueOf(id)});

        if(cursor.moveToFirst()){

            task = getTaskFromDB(cursor,id);
        }

        cursor.close();

        return  task;
    }

/*    public long getCount(){
        return DatabaseUtils.queryNumEntries(database,DATABASE_TABLE);
    }*/

    private Task getTaskFromDB(Cursor cursor, long id){

        if(id<0) {//Is created or not

            id = cursor.getLong(cursor.getColumnIndex(COLUMN_ID));
        }

        long classroom = cursor.getLong(cursor.getColumnIndex(COLUMN_CLASSROOM));
        String subject = cursor.getString(cursor.getColumnIndex(COLUMN_SUBJECT));
        String lecturer = cursor.getString(cursor.getColumnIndex(COLUMN_LECTURER));
        long beginOfSubject = cursor.getLong(cursor.getColumnIndex(COLUMN_BEGINOFSUBJECT));
        long endOfSubject = cursor.getLong(cursor.getColumnIndex(COLUMN_ENDOFSUBJECT));
        long date = cursor.getLong(cursor.getColumnIndex(COLUMN_DATE));

        return new Task(id,classroom,subject,lecturer,beginOfSubject,endOfSubject,date);
    }

    private ContentValues createContentValuesFor(Task task){
        ContentValues cv = new ContentValues();

        cv.put(COLUMN_CLASSROOM, task.getClassroom());
        cv.put(COLUMN_SUBJECT, task.getSubject());
        cv.put(COLUMN_LECTURER, task.getLecturer());
        cv.put(COLUMN_BEGINOFSUBJECT, task.getBeginingOfSubject());
        cv.put(COLUMN_ENDOFSUBJECT, task.getEndingOfSubject());
        cv.put(COLUMN_DATE, task.getDate());

        return cv;
    }

}
