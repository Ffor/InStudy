package com.gsp.instudy;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

public class DatabaseHelper extends SQLiteOpenHelper {

    private static final String DATABASE_NAME = "instudy";
    private static final int DATABASE_VERSION = 1;

    private static final String DATABASE_TABLE_TASKS = "tasks";
    private static final String DATABASE_TABLE_DATE = "date";

    private static final String COLUMN_ID = "_id";
    private static final String COLUMN_CLASSROOM = "classroom";
    private static final String COLUMN_SUBJECT = "subject";
    private static final String COLUMN_LECTURER = "lecturer";
    private static final String COLUMN_BEGINOFSUBJECT = "beginofsubject";
    private static final String COLUMN_ENDOFSUBJECT = "endofsubject";
    private static final String COLUMN_DATE = "date";

    private static final String DATABASE_CREATE_TABLE_TASKS =
            "CREATE TABLE " + DATABASE_TABLE_TASKS + "("
                    + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + COLUMN_CLASSROOM + " INTEGER, "
                    + COLUMN_SUBJECT + " TEXT, " + COLUMN_LECTURER + " TEXT, "
                    + COLUMN_BEGINOFSUBJECT + " INTEGER, "
                    + COLUMN_ENDOFSUBJECT + " INTEGER, " + COLUMN_DATE + " INTEGER " + ");";

    private static final String DATABASE_CREATE_TABLE_DATE =
            "CREATE TABLE " +DATABASE_TABLE_DATE + " ( "
                    + COLUMN_ID + " INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + COLUMN_DATE + " INTEGER " + ");";

    public DatabaseHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {

        db.execSQL(DATABASE_CREATE_TABLE_TASKS);
        db.execSQL(DATABASE_CREATE_TABLE_DATE);
    }
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion,  int newVersion) {
    }
}